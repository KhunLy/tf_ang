import { Component, OnInit } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  public items: NbMenuItem[] = [
    { title: 'Home', link: '/home', icon: 'home' },
    { title: 'About', link: '/about', icon: 'question-mark' },
    { title: 'Demo', icon: 'folder', children: [
      { title: 'Demo 1 - Binding One Way', link: '/demo/demo1' },
      { title: 'Demo 2 - Event', link: '/demo/demo2' },
    ] },
  ];

  constructor() { }

  ngOnInit() {
  }

}
