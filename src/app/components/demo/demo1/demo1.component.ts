import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo1',
  templateUrl: './demo1.component.html',
  styleUrls: ['./demo1.component.scss']
})
export class Demo1Component implements OnInit {

  
  public maVariable : string;
  

  constructor() { }

  ngOnInit() {
    this.maVariable = "World";
    setTimeout(() => {
      this.maVariable = "Khun";
    }, 5000);
  }

}
